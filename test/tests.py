import pytest
from work_with_csv import ReadCSV


@pytest.fixture
def correct_file_for_constructor(tmp_path):
    tname = "test_const"
    tpath = tmp_path.joinpath(tname)
    with open(tpath, "w", encoding="utf-8") as f:
        f.writelines(
            "2;3;4;5\n"
            'eddie"a;te";2\n'
            "dynamite"
        )
    return tpath


@pytest.fixture
def empty_file_for_constructor(tmp_path):
    tname = "test_const"
    tpath = tmp_path.joinpath(tname)
    with open(tpath, "w", encoding="utf-8") as f:
        f.write("")
    return tpath


@pytest.fixture
def summable_file_for_constructor(tmp_path):
    tname = "test_const"
    tpath = tmp_path.joinpath(tname)
    with open(tpath, "w", encoding="utf-8") as f:
        f.writelines(
            "2\n"
            "3\n"
            "4\n"
        )
    return tpath


@pytest.fixture
def notsummable_file_for_constructor(tmp_path):
    tname = "test_const"
    tpath = tmp_path.joinpath(tname)
    with open(tpath, "w", encoding="utf-8") as f:
        f.writelines(
            "2\n"
            "3;\n"
            "4\n"
        )
    return tpath

@pytest.fixture
def invalid_cav(tmp_path):
    tname = "test_const"
    tpath = tmp_path.joinpath(tname)
    with open(tpath, "w", encoding="utf-8") as f:
        f.writelines(
            '2"""\n'
            "3;\n"
            "4\n"
        )
    return tpath


@pytest.fixture
def difficult(tmp_path):
    tname = "test_const"
    tpath = tmp_path.joinpath(tname)
    with open(tpath, "w", encoding="utf-8") as f:
        f.writelines(
            "2;3;4;5\n"
            'eddie"a"";eddie"a"";2\n'
            "dynamite\n"
        )
    return tpath


class TestConstructor:
    def test_constructor_correct(self, correct_file_for_constructor):
        obj = ReadCSV(correct_file_for_constructor)
        assert obj._rows == ["2;3;4;5", 'eddie"a;te";2', "dynamite"]
        assert obj.is_summable is False

    def test_constructor_error_runtime(self):
        with pytest.raises(RuntimeError):
            obj = ReadCSV("./wfwjfw")

    def test_constructor_error_buffer(self, empty_file_for_constructor):
        with pytest.raises(BufferError):
            obj = ReadCSV(empty_file_for_constructor)

    def test_constructor_issummable(self, summable_file_for_constructor):
        obj = ReadCSV(summable_file_for_constructor)
        assert obj.is_summable is True

    def test_constructor_notsummable(self, notsummable_file_for_constructor):
        obj = ReadCSV(notsummable_file_for_constructor)
        assert obj.is_summable is False


class TestGetItem:
    def test_getitem_correct(self, summable_file_for_constructor):
        obj = ReadCSV(summable_file_for_constructor)
        assert obj[1] == "3"

    def test_getitem_outrange(self, summable_file_for_constructor):
        obj = ReadCSV(summable_file_for_constructor)
        with pytest.raises(IndexError):
            obj[10101001]


class TestAdd:
    def test_add_notsummable(self, summable_file_for_constructor, notsummable_file_for_constructor):
        a = ReadCSV(summable_file_for_constructor)
        b = ReadCSV(notsummable_file_for_constructor)
        with pytest.raises(ArithmeticError):
            a + b

    def test_add_same_sizes(self, summable_file_for_constructor):
        a = ReadCSV(summable_file_for_constructor)
        b = ReadCSV(summable_file_for_constructor)
        c = a + b
        assert c._rows == ["4", "6", "8"]

    def test_add_different_sizes(self, summable_file_for_constructor):
        a = ReadCSV(summable_file_for_constructor)
        a._rows.append("5")
        b = ReadCSV(summable_file_for_constructor)
        c = a + b
        assert c._rows == ["4", "6", "8"]


class TestGetValue:
    def test_getvalue_correct_nocav(self, correct_file_for_constructor):
        a = ReadCSV(correct_file_for_constructor)
        assert a.get_value(0, 1) == "3"

    def test_getvalue_correct_withcav(self, correct_file_for_constructor):
        a = ReadCSV(correct_file_for_constructor)
        assert a.get_value(1, 1) == "2"

    def test_getvalue_runtime_index(self, correct_file_for_constructor):
        with pytest.raises(RuntimeError):
            a = ReadCSV(correct_file_for_constructor)
            a.get_value(2141, 1241)

    def test_getvalue_runtime_cav(self, invalid_cav):
        with pytest.raises(RuntimeError):
            a = ReadCSV(invalid_cav)
            a.get_value(0, 0)

    def test_getvalue_difficult(self, difficult):
        a = ReadCSV(difficult)
        assert a.get_value(1, 1) == "2"
