"""KR 2"""
#test
import argparse
import os
import sys


class ReadCSV:
    """Class from KR 2"""
    def __init__(self,
                 filename: str):
        """Конструктор класса ReadCSV, построчно считывающий исходный файл
        @param filename: str, - путь до исходного CSV-файла
        @raises RuntimeError в случае проблем с чтением исходного файла
        @raises BufferError с выводом сообщения 'no rows in CSV' в случае, если исходный файл пуст

        Объект класса должен иметь 2 атрибута
        - _rows: list, - список считанных строк исходного файла
        - is_summable: bool, - индикатор суммируемости исходного файла
            == True, если исходный CSV-файл состоит из только 1-го столбца,
                     причём содержит только целые числа
            == False, во всех остальных ситуациях

        В случае, если возникают проблемы с чтением исходного CSV-файла, требуется генерировать
        исключение RuntimeError
        В случае отсутствия строк в исходном файле, требуется генерировать исключение BufferError
        с сообщением 'no rows in CSV'

        ВАЖНО отметить, что в считываемых строках исходного CSV-файла требуется обрезать начальные
        и конечные пробельные символы
        """
        try:
            with open(os.path.abspath(filename), "r", encoding="utf-8") as work_file:
                self._rows = [s.strip() for s in work_file.readlines()]
                if not self._rows:
                    raise BufferError("no rows in csv")
        except (PermissionError, IsADirectoryError, FileNotFoundError) as exc:
            raise RuntimeError from exc

        for str_ in self._rows:
            if ";" in str_:
                self.is_summable = False
                break

            try:
                int(str_)
                self.is_summable = True
            except ValueError:
                self.is_summable = False
                break

    def get_rows(self):
        """
        Returns:
            A list of rows of a file
        """
        return self._rows

    def __getitem__(self,
                    index: int) -> str:
        """Оператор индексирования по строкам исходного CSV-файла
        @param index: int, - индекс строки в исходном CSV-файле
        @return: str, - искомая строка из входного CSV-файла
        """
        return self._rows[index]

    def __add__(self, other):
        """Оператор сложения 2-х суммируемых (значение True у атрибута is_summable) CSV-файлов
        @param other: ReadCSV, - объект класса ReadCSV (считанный CSV-файл)
        @return: ReadCSV, - объект класса ReadCSV, представляющий собой сумму 2-х CSV-файлов
        @raises ArithmeticError с текстом 'can't summarize non-summable files' в случае,
            если сложить 2 исходных CSV-файла корректно нельзя (хотя бы один из них не-суммируемый)

        Результатом сложения 2-х суммируемых CSV-файлов является объект класса ReadCSV,
        длина атрибута _rows которого равна наименьшей длине атрибута _rows у складываемых файлов,
        а значения на соответствующих позициях равны сумме значений на тех же позициях
        в исходных файлах, т.е. чтобы получить 2-ой элемент атрибута _rows результирующего объекта,
        требуется сложить 2-ой элемент атрибута _rows объектов self и other
        В случае, если один из файлов не является суммируемым, генерировать
        исключение ArithmeticError с текстом 'can't summarize non-summable files'
        """
        if self.is_summable is False or other.is_summable is False:
            raise ArithmeticError("can't summarize non-summable files")
        with open("added", "w", encoding="utf-8") as result_file:
            min_len = min(len(self._rows), len(other._rows))

            for index in range(min_len):
                result_file.write(f"{int(self._rows[index]) + int(other._rows[index])}\n")
        return ReadCSV("added")

    def get_value(self,
                  row_index: int,
                  column_index: int) -> str:
        """Получение строкового значения в соответствующей ячейке входного CSV-файла
        @param row_index: int, - индекс строки CSV-файла
        @param column_index: int, - индекс столбца CSV-файла
        @return: str, - строковое значение в соответствующей ячейке
        @raises: RuntimeError в случае ошибок индексирования ячейки
        @raises: RuntimeError в случае отсутствия парных закрывающихся двойных ковычек

        Для получения значения ячейки в соответствующей строке исходного CSV-файла требуется
        разбить её с учётом того, что разделителем является символ ';'

        В случае ошибок индексирования по строкам или столбцам исходного CSV-файла
        генерировать исключение RuntimeError

        В строке могут присутствовать двойные кавычки: подстрока, обрамлённое с 2-х сторон
        двойными кавычками (символ ") должно интерпретироваться как одно значение
        В случае, если в строке для какой-то из открывающихся двойных ковычек отсутствует
        её закрывающаяся пара, генерировать исключение RuntimeError
        """
        try:
            return self.get_list_of_columns(row_index)[column_index]
        except IndexError as exc:
            raise RuntimeError from exc

    def get_list_of_columns(self, row_index: int) -> list:
        """
        Method that returns parsed string of an object
        Args:
            row_index: index of a row of a file needed to be parsed

        Returns:
            A list from a string
        """
        result_row = []
        went_indexes = []
        count = 0
        splitted_row = self._rows[row_index].split(";")
        for elem in splitted_row:
            count += elem.count('"')
        if count % 2 == 1:
            raise RuntimeError
        for (separated_index, separated) in enumerate(splitted_row):
            separated = str(separated)
            if '"' in separated and separated.count('"') % 2 == 1:
                for (second_index, second) in enumerate(splitted_row[separated_index + 1:]):
                    if '"' in second and second.count('"') % 2 == 1:
                        if separated_index not in went_indexes:
                            finish_index = second_index + separated_index + 2
                            new_row = splitted_row[separated_index:finish_index]
                            new_element = (";".join(new_row))

                            went_indexes.extend(range(separated_index, finish_index))
                            result_row.append(new_element.strip('"'))
                            break
            else:
                if separated_index not in went_indexes:
                    result_row.append(separated.strip('"'))
        return result_row


def parse_cmd_args(cmd_args: list) -> argparse.Namespace:
    """Разбор аргументов командной строки
    @param cmd_args: list, - список аргументов командной строки
    @return: argparse.Namespace, -
        subparser_name: str, - название режима работы скрипта ("визуализация" или "суммирование")
            - visualize:
                csv_filename: str, - путь до исходного CSV-файла
                rows_quantity: int, - ограничение на количество выводимых строк исходного CSV-файла
            - summarize:
                csv_filenames: list, - список путей до суммируемых CSV-файлов
    """
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help="Featured commands", dest="subparser_name")
    summarize = subparsers.add_parser(
        name="summarize",
        help="summarizing a list of files"
    )
    summarize.add_argument(
        "csv_filenames",
        nargs="*"
    )
    visualize = subparsers.add_parser(
        name="visualize",
        help="visualizing an amount of rows from a file"
    )
    visualize.add_argument(
        "--rows-quantity", "-r",
        metavar="NUMBER",
        help="Amount of rows to be visualised",
        type=int
    )
    visualize.add_argument(
        "--path-to-csv", "-p",
        metavar="STRING",
        dest="csv_filename",
        help="Path to csv",
        type=str
    )
    return parser.parse_args(cmd_args)


def summarize_csvs(csv_filenames: list) -> list:
    """Суммирование целочисленных значений единственного столбца входных CSV-файлов
    @param csv_filenames: list, - список путей до CSV-файлов (хотя бы 1)
    @return: list, - список целых чисел с результатом сложения 1-го столбца CSV-файлов
    """
    first = None
    index = None
    for (index, csv_name) in enumerate(csv_filenames):
        obj = ReadCSV(csv_name)
        if obj.is_summable:
            first = obj
            break
    if first is not None:
        for elem in csv_filenames[index + 1:]:
            elem_obj = ReadCSV(elem)
            if elem_obj.is_summable:
                first = first + elem_obj
        return [int(i) for i in first.get_rows()]
    return []


def visualize_csv(csv_filename: str,
                  rows_quantity: int) -> None:
    """Визуализация данных из входного CSV-файла
    @param csv_filename: str, - путь до CSV-файла
    @param rows_quantity: int, - количество строк для чтения из входного файла
    """
    obj = ReadCSV(csv_filename)
    if rows_quantity <= len(obj.get_rows()):
        right_quantity = rows_quantity
    else:
        right_quantity = len(obj.get_rows())
    for str_index in range(1, right_quantity + 1):
        print(f'row {str_index}: "{obj[str_index - 1]}"')
        result_row = obj.get_list_of_columns(str_index - 1)
        for elem in result_row:
            print("\t" + elem)


def main():
    """Main Function"""
    cli_args = sys.argv[1:]

    intermixed_args = parse_cmd_args(cli_args)
    if intermixed_args.subparser_name == "summarize":
        print(summarize_csvs(intermixed_args.csv_filenames))
    if intermixed_args.subparser_name == "visualize":
        visualize_csv(intermixed_args.csv_filename, intermixed_args.rows_quantity)


if __name__ == "__main__":
    main()
