# Модуль pylint_dir.py
Модуль, запускающий pylint для всех файлов каталога

# Запуск:
```bash
python3 pylint_dir.py INPUT_DIR
```

# Запуск тестов:
```bash
pytest test_pylint_dir.py
coverage report -m test_pylint_dir.py

# another way
coverage run --source=. -m pytest test_pylint_dir.py
coverage report  -m
```
