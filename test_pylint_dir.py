# pylint: skip-file
import pytest
import os
from pylint_dir import DirectoryLinter


def test_constructor():
    obj = DirectoryLinter("./test")

    assert obj.work_dir == os.path.abspath("./test")
    assert obj.files_amount == 0
    assert obj.pylint_score == 0


def test_walk_dir_empty_dir(tmpdir):
    obj = DirectoryLinter(tmpdir)

    real_ans = obj.walk_dir()
    assert real_ans == 0.00


def test_walk_dir_real_dir():
    obj = DirectoryLinter("./test")

    real_ans = obj.walk_dir()

    assert real_ans == 6.53

