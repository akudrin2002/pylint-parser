"""
Module with parsing pylint output
USAGE: python3 pylint_dir.py INPUT_DIR
"""
import os
import subprocess
import re
import sys

import click
from loguru import logger

logger.remove()
logger.add(
    sink=sys.stdout,
    colorize=True,
    # pylint: disable=line-too-long
    format="<yellow>{time:YYYY-MM-DD at HH:mm:ss}</yellow> | <level>{level}</level>: <level>{message}</level>"
    # pylint: enable=line-too-long
)
logger = logger.opt(colors=True)


# pylint: disable=too-few-public-methods
class DirectoryLinter:
    """Class with realisation of a directory linter"""
    def __init__(self, input_dir: str):
        self.work_dir = os.path.abspath(input_dir)
        self.files_amount = 0
        self.pylint_score = 0

    def walk_dir(self) -> float:
        """
        Methods that walks through files and collects their pylint scores
        Returns: pylint score of a directory
        """
        for root, _, files in os.walk(self.work_dir):
            for name in files:
                filepath = os.path.join(root, name)
                self._lint_file(filepath)
        if self.files_amount != 0:
            return round(self.pylint_score / self.files_amount, 2)
        return 0.00

    def _lint_file(self, filepath: str) -> None:
        if filepath[-3:] == ".py":
            with subprocess.Popen(f"pylint {filepath}", stdout=subprocess.PIPE) as desc:
                pout = desc.stdout.read().decode()
            for line in pout.split("\n"):
                if "Your code has been rated at" in line:
                    score = re.findall(r"\d.+\d\d", line)[0][:4]
                    logger.info(f"Score for <green>{filepath}</green>: <blue>{score}</blue>")
                    self.pylint_score += float(score)
                    self.files_amount += 1

# pylint: disable=too-few-public-methods


@click.command()
@click.argument("input_dir")
def main(input_dir):
    """Main function"""
    linter = DirectoryLinter(input_dir)
    logger.info(f"Directory: <green>{linter.work_dir}</green>")
    scored = linter.walk_dir()
    logger.success(f"Result pylint score: <blue>{scored}</blue>")


if __name__ == '__main__':
    # pylint hack:
    # https://stackoverflow.com/a/49680253/19204439
    # pylint: disable=no-value-for-parameter
    main()
    # pylint: enable=no-value-for-parameter
